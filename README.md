# pdf-parser-nodejs

This is the working proof of concept for parsing PDFs concurrently
per a specific parsing function, as per the series I've written on
the subject. It's the result of a few weeks of focused learning, and
I can tell you right now there are a few drawbacks and a few
strengths to it as well. For a deeper look go ahead and look through
the repo, or read through the series:

 * Post 1: [How to parse hundreds of PDFs in a blink with NodeJS Streams](https://www.fourzerofour.pw/posts/pdf-parser-nodejs/)
 * Post 2: [How to parse structured PDFs with NodeJS](https://www.fourzerofour.pw/posts/pdf-parser-nodejs-2/)
 * Post 3: [How to scale NodeJS PDF parsing: lessons learned](https://www.fourzerofour.pw/posts/pdf-parser-nodejs-3/)

#### What's not covered in the blog posts?

Just one file - [`masscopy.sh`](https://gitlab.com/fourzerofour/pdf-parser-nodejs/blob/master/masscopy.sh)

This file just copies the PDFs in the ./data directory, over and over again,
to ensure that we've got some workable dummy data to play with. A simple
hack, but hey, it works enough for a proof of concept.
